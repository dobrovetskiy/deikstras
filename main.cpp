#include <iostream>
#include <stdio.h>
#include <memory.h>

using namespace std;

const int MAX = 10;
int n,source;


int weight[MAX][MAX], used[MAX], d[MAX];

void Init(int n)
{
        for(int i = 1; i <= n; i++)
        {
                d[i] = -1;   
                for(int j = 1; j <= n; j++)
                        if(i == j) weight[i][j] = 0;
                        else weight[i][j] = -1;
        
        }
        memset(used, 0, sizeof(used));
        d[source] = 0;
}



void Relax(int i, int j)
{
        if (((d[j] > d[i] + weight[i][j]) or d[j] < 0) and weight[i][j] > 0) d[j] = d[i] + weight[i][j];
        cout<<i<<" "<<d[i]<<" "<<d[j]<<" "<<j<<" "<<weight[i][j]<<endl;
       
}


int Find_Min(void)
{
        int i ,tmp_min = 0, min = 1000000;
        
        for(i=1;i<=n;i++)
        {
                if (!used[i] and (d[i] < min) and (d[i] >= 0)) {
                        min = d[i], tmp_min = i;
                }
        }
        return tmp_min;
}

int main(void)

{
 
        cin>>n>>source;

        Init(n);
        
        int i,j;
        while( true)
        {
                cin>>i;
                if(i == 0) break;
                cin>>j;
                cin>>weight[i][j];
                weight[j][i] = weight[i][j];
               
        }    
        

        for (i = 1; i < n; i++)
        {
                int tmp_min = Find_Min();
                if (tmp_min == 0) break;
                for (j = 1; j <= n; j++)
                {
                         if (!used[j]) Relax(tmp_min,j);
                          
                }
                used[tmp_min] = 1;
        }

        for (i = 1; i <= n; i++)
        {
                cout<<"From source "<<source<<" to destination "<<i;
                if (d[i] < 0) cout<<" there is no edge's way"<<endl;
                else cout<<" distance is "<<d[i]<<endl;         
        }
  
        return 0;

}
